from bs4 import BeautifulSoup
import urllib.request
from urllib.parse import *
import pafy
import youtube_dl
from playlist_db import *

class Youtube_mp3():
    def __init__(self):
        self.searchReturn = {}
        self.searchReturn_names = {}
        self.current_song = tuple()
        self.local_files = {}
        self.playlist_names = {}
        self.current_playlist = []
        self.music_folder = 'music_folder/'
        self.ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl': f"{self.music_folder}%(title)s.%(ext)s"
        }
        self.playlist_db = PlaylistDb()
        self.load_playlists_names(True)


    async def url_search(self, search_string, max_search):
        textToSearch = search_string
        query = urllib.parse.quote(textToSearch)
        url = "https://www.youtube.com/results?search_query=" + query
        response = urllib.request.urlopen(url)
        html = response.read()
        soup = BeautifulSoup(html, 'lxml')
        i = 1
        for vid in soup.findAll(attrs={'class':'yt-uix-tile-link'}):
            if len(self.searchReturn) < max_search:
                self.searchReturn[i] = 'https://www.youtube.com' + vid['href']
                i += 1
            else:
                break


    async def get_search_items(self, max_search):
        if self.searchReturn != {}:
            i = 1
            for url in self.searchReturn.values():
                try:
                    info = pafy.new(url)
                    self.searchReturn_names[i] = info.title
                    i += 1
                
                except ValueError:
                    break

            return self.searchReturn_names

    async def download_song_from_url(self, num):
        url = self.searchReturn[int(num)]
        print(f"num : {num}, url : {url}, song_name : {self.searchReturn_names[int(num)]}")
        song_name = self.searchReturn_names[int(num)]
        print("Downloading: {0}.".format(song_name))
        with youtube_dl.YoutubeDL(self.ydl_opts) as ydl:
            ydl.download([url])
    
    def load_playlists_names(self, force_load=False):
        if self.playlist_names == {} or force_load :
            k = 1
            for playlist in self.playlist_db.playlist_data.playlists:
                self.playlist_names[k] = playlist.name
                k = k + 1

    def save_current_playlist_with_name(self, name):
        if name not in self.playlist_names.values() and self.current_playlist != []:
            self.playlist_db.playlist_data.playlists.append(Playlist(name, self.current_playlist))
            self.playlist_db.dumpSavedPlaylist()
            self.load_playlists_names(True)
        else:
            print("i dont respect condition to save")
    
    async def swap_current_playlist(self, playlist_number):
        self.current_playlist = self.playlist_db.get_list_of_song_with_playlist_number(playlist_number)
    
    async def delete_playlist_with_number(self, playlist_number):
        self.playlist_db.delete_playlist_with_number(playlist_number)
        self.load_playlists_names(True)
       

