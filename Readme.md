# Python discord bot

This is a simple discord bot

# How to use it

## Set up your env and run the app !

- Ensure you got python 3.7 or higher and pip installed
- Clone the project
- Run those commands (for windows) :

```bash
cd my_project_folder
python -m venv venv
.\venv\Scripts\activate.bat
pip install -r requirements.txt
```

- Make a discord bot on [discord dev portal](https://discordapp.com/developers/applications/)
- Connect on discord on your server in any vocal chat
- Create a .env file at the root of your project and follow .envexample file to write whats necessary

- Run the app :

```bash
python -m main.py
```

- In discord chat write `#search`

- Follow the bot instructions