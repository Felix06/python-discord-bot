# main.py
import os
import asyncio
import random
import pafy
import discord
import util
from dotenv import load_dotenv
from youtube_mp3 import Youtube_mp3
from song_dataclasses import Song

load_dotenv()
token = os.getenv('DISCORD_TOKEN')
ffmpeg_executable = os.getenv('ffmpeg_executable')

client = discord.Client()
youtube_mp3 = Youtube_mp3()

max_search = 5
is_handling_search = False
is_in_pause = False
skip = False
switch_playlist = False
current_voice_channel = None
stopped = False
vc = None

async def connect_to_user_voice_channel(message_channel, user):
    global vc
    voice_channel = user.voice.channel
    #check if not currently in this voice channel test
    if voice_channel != None and vc == None:
        channel = voice_channel.name
        vc = await voice_channel.connect()
        return vc
    else:
        if not vc.is_playing():
            await message_channel.send("please enter a vocal channel.")
        return vc

async def swap_voice_channel(message_channel, user):
    global vc
    voice_channel = user.voice.channel
    #externise this in another function
    if voice_channel != None and vc != None:
        vc.disconnect()
        channel = voice_channel.name
        vc = await voice_channel.connect()

async def play_song(song):
    global vc
    global skip
    global youtube_mp3
    if vc.is_playing():
        vc.stop()
    vc.play(discord.FFmpegPCMAudio(source=f"{youtube_mp3.music_folder}{song.name}", executable=ffmpeg_executable), after=lambda e: print('done', e))
    #try remove is in pause to implement skip
    while vc.is_playing() or is_in_pause == True:
        await asyncio.sleep(1)
    skip = False
    vc.stop()

async def play_song_or_put_in_queue_handle_play_next(song):
    global vc
    global youtube_mp3
    global stopped
    stopped = False
    if song not in youtube_mp3.current_playlist:
        youtube_mp3.current_playlist.append(song)
    if not vc.is_playing():
        song_number = return_place_find_in_list(youtube_mp3.current_playlist, song)
        youtube_mp3.current_song = (song_number, song)
        await play_song(song)
        if not youtube_mp3.current_playlist[len(youtube_mp3.current_playlist) - 1].name.startswith(song.name) and not stopped:
            next_song_nb = song_number + 1
            next_song = youtube_mp3.current_playlist[next_song_nb]
            await play_song_or_put_in_queue_handle_play_next(next_song)

async def if_vc_is_none_connect_user_channel(message, author):
    global vc
    if vc is None:
        return await connect_to_user_voice_channel(message.channel, author)
    else:
        return vc

async def display_local_music_file(message):
    global youtube_mp3
    dir_path = os.path.join(os.path.dirname(__file__), youtube_mp3.music_folder)
    youtube_mp3.local_files = {}
    j = 1

    for root, dirs, files in os.walk(dir_path):
        for file in files:
            youtube_mp3.local_files[j] = str(file)
            await message.channel.send("{0}. {1}".format(j, str(file)))
            j = j + 1

async def search_song_in_local_dir(song_name):
    global youtube_mp3
    dir_path = os.path.join(os.path.dirname(__file__), youtube_mp3.music_folder)
    for root, dirs, files in os.walk(dir_path): 
        for file in files: 
            if file.startswith(song_name):
                return str(file)

def return_place_find_in_list(list_songs, song):
    i = 0
    for sg in list_songs:
        if song.name.startswith(sg.name):
            return i
        i = i + 1

async def handle_bad_response_song_number_recursivly(min_range, max_range, message, client):
    input_is_not_int = False
    user_input = 0
    try:
        user_input = int(message.content)
    except:
        input_is_not_int = True

    if user_input > max_range or user_input <= min_range or input_is_not_int:
        await message.channel.send(f"Bad input. Please something between {min_range + 1} and {max_range}.")
        await message.channel.send("New input :")
        message = await client.wait_for("message")
        return await handle_bad_response_song_number_recursivly(min_range, max_range, message, client)
    return user_input

async def handle_interactiv_youtube_search(message, user):
    global is_handling_search
    global is_in_pause
    global vc
    global youtube_mp3
    global max_search
    await message.channel.send('Welcome to the Youtube-Mp3 player.')
    search = ''
    
    is_handling_search = True
    await message.channel.send("Youtube Search: ")
    search_message = await client.wait_for('message')
    search = search_message.content
    youtube_mp3.searchReturn = {}
    youtube_mp3.searchReturn_names = {}

    if search == 'q':
        await message.channel.send("Ending Youtube-Mp3 player.")
        is_handling_search = False
        return

    await youtube_mp3.url_search(search, max_search)
    searched_items = await youtube_mp3.get_search_items(max_search)

    multiline_response = ""
    for item_key in searched_items:
        multiline_response = f"""{multiline_response}
{item_key}. {searched_items[item_key]}"""
    
    multiline_response = f"""{multiline_response}
Input song number:"""
    await message.channel.send(f"{multiline_response}")
    
    song_number_message = await client.wait_for('message')
    song_number = await handle_bad_response_song_number_recursivly(0, max_search, song_number_message, client)
    
    #check if song number is a number in possible ranges
    await youtube_mp3.download_song_from_url(song_number)
    await connect_to_user_voice_channel(message.channel, user)
    song_name = await search_song_in_local_dir(searched_items[int(song_number)])
    
    is_handling_search = False

    await play_song_or_put_in_queue_handle_play_next(Song(song_name))

@client.event
async def on_ready():
    global vc
    print(f'{client.user.name} has connected to Discord!')
    print(client.voice_clients)
    # vc = client.user.voice.channel

@client.event
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel.send(
        f'Hi {member.name}, welcome to Fefouland server bitcccchhhhsssss!'
    )

## Just for testing the bot
async def brooklyn_99_quotes(message):
    brooklyn_99_quotes = [
        'I\'m the human form of the 💯 emoji.',
        'Bingpot!',
        (
            'Cool. Cool cool cool cool cool cool cool, '
            'no doubt no doubt no doubt no doubt.'
        ),
    ]
    response = random.choice(brooklyn_99_quotes)
    await message.channel.send(response)

async def stop_bot_song(message):
    global vc
    global stopped
    if vc != None:
        stopped = True
        vc.stop()
    else:
        await message.channel.send("Bot is not in any voice channel.")

async def pause_bot_song(message):
    global vc
    global is_in_pause
    if vc != None:
        vc.pause()
        is_in_pause = True
    else:
        await message.channel.send("Bot is not in any voice channel.")

async def resume_bot_song(message):
    global vc
    global is_in_pause
    if vc != None:
        vc.resume()
        is_in_pause = False
    else:
        await message.channel.send("Bot is not in any voice channel.")

async def skip_bot_song(message):
    global vc
    global skip
    global stopped
    if vc != None:
        (curr_song_number, curr_song) = youtube_mp3.current_song
        next_song_nb = curr_song_number + 1
        stopped = True
        vc.stop()
        await play_song_or_put_in_queue_handle_play_next(youtube_mp3.current_playlist[next_song_nb])

async def disconnect_bot(message):
    global vc
    if vc != None:
        stopped = True
        vc.stop()
        await vc.disconnect()
        vc = None
        stopped = False
    else:
        await message.channel.send("Bot is not in any voice channel.")

async def handle_recursivly_empty_matches(message, client, list_to_match):
    user_message = await client.wait_for("message")
    song_match = user_message.content
    matches_playlist = util.get_matches_in_list_for_songs(list_to_match, song_match)
    if matches_playlist is []:
        await message.channel.send("No  songs found for keyword. Please provide a match :")
        return await handle_recursivly_empty_matches(message, client, list_to_match)
    else:
        return matches_playlist

async def display_current_playlist(message):
    global youtube_mp3

    multiline_response = ""
    if youtube_mp3.current_playlist != []:
        l = 1
        for song in youtube_mp3.current_playlist:
            multiline_response = f"""{multiline_response}
{l}. {song}"""
            l = l + 1
        await message.channel.send(multiline_response)

async def handle_playlist_keyword(message, author):
    global vc
    global youtube_mp3
    global stopped
    multiline_response = "      "

    if message.content == "#playlist --all":
        if youtube_mp3.playlist_names != {}:
            for k in youtube_mp3.playlist_names:
                multiline_response = f"""{multiline_response}
{k}. {youtube_mp3.playlist_names[k]}"""
            return await message.channel.send(multiline_response)

    if message.content == "#playlist --new":
        await message.channel.send("Input name of new playlist")
        playlist_name_message = await client.wait_for('message')
        playlist_name = playlist_name_message.content
        return youtube_mp3.save_current_playlist_with_name(playlist_name)

    if message.content == "#playlist --current":
        return await display_current_playlist(message)
    
    if message.content == "#playlist --switch":
        await message.channel.send("Please enter playlist number :")
        user_message = await client.wait_for("message")
        playlist_number = await handle_bad_response_song_number_recursivly(0, len(youtube_mp3.playlist_db.playlist_data.playlists), user_message, client)
        switch_playlist = True
        if playlist_number == 1:
            print("reloading local playlist")
            youtube_mp3.playlist_db.update_local_playlist()
        await youtube_mp3.swap_current_playlist(playlist_number)
        vc = await if_vc_is_none_connect_user_channel(message, author)
        stopped = True
        vc.stop()
        await play_song_or_put_in_queue_handle_play_next(youtube_mp3.current_playlist[0])

    if message.content == "#playlist --play":
        await message.channel.send("Please enter song number in playlist :")
        user_message = await client.wait_for("message")
        song_number = await handle_bad_response_song_number_recursivly(0, len(youtube_mp3.current_playlist), user_message, client)
        vc = await if_vc_is_none_connect_user_channel(message, author)
        stopped = True
        vc.stop()
        await play_song_or_put_in_queue_handle_play_next(youtube_mp3.current_playlist[song_number - 1])

    if message.content == '#playlist --regroup':
        await message.channel.send("Please enter a regrouping match for songs :")
        new_playlist = await handle_recursivly_empty_matches(message, client, youtube_mp3.current_playlist)
        youtube_mp3.current_playlist = new_playlist
        vc = await if_vc_is_none_connect_user_channel(message, author)
        await display_current_playlist(message)
        stopped = True
        vc.stop()
        await play_song_or_put_in_queue_handle_play_next(youtube_mp3.current_playlist[0])

    if message.content == "#playlist --delete":
        await message.channel.send("Please enter the number of the paylist you wish to delete :")
        user_message = await client.wait_for("message")
        playlist_to_delete_number = await handle_bad_response_song_number_recursivly(0, len(youtube_mp3.playlist_names), user_message, client)
        await youtube_mp3.delete_playlist_with_number(playlist_to_delete_number)

async def help_dbot(message):
    await message.channel.send("""
    ### This is a work in progress. There is a lot of bug left. ###
### Please contact me at vadcard.felix@gmail.com for any issues. ###

    keywords :
    - zic bot :
        #search:
            start interactive search on youtube with the bot
            this will add the searched song at the end of current playlist
        #pause:
            pause the current song
        #resume:
            resume current song
        #stop:
            stop current song
        #disconnect:
            disconnect bot from current voice channel
        #skip:
            play next song in playlist
        #playlist:
            WIP playlist interaction and play
            options :
                --all :
                    display all playslists names and corresponding number (usefull to switch playlist)
                --current :
                    display all songs in current playlist (usefull to play specific song in playlist)
                --new :
                    create new playlist with the current one
                --switch :
                    switch current playlist by providing playlist number
                --play :
                    play specifique song in playlist by providing song number
                --regroup :
                    assign new current playlist as the result of a regrouping matches of song name/author of previous current playlist
                --delete :
                    delete playlist providing playlist number
    """)

#add other keywords to check for saved playlist, save a new playlist, look for local file and  be able to delete it and aslo voice control (volume, stop, pause, skip etc ...)
@client.event
async def on_message(message):
    global is_handling_search
    global vc
    
    try:
        author = message.author
        if author == client.user:
            return

        if is_handling_search is False:

            if message.content == '99!':
                return await brooklyn_99_quotes(message)

            if message.content == "#stop":
                return await stop_bot_song(message)

            if message.content == "#resume":
                return await resume_bot_song(message)

            if message.content == "#pause":
                return await pause_bot_song(message)

            if message.content == "#skip":
                return await skip_bot_song(message)

            if message.content == "#local_files":
                return await display_local_music_file(message)

            if message.content.startswith("#purge_local_files"):
                pass

            if message.content.startswith("#playlist"):
                return await handle_playlist_keyword(message, author)

            if message.content == "#help":
                return await help_dbot(message)

            if message.content == "#disconnect":
                return await disconnect_bot(message)

            if message.content == "#search":
                return await handle_interactiv_youtube_search(message, author)

            if message.content == "#stop_bot":
                return await client.logout()
    except:
        return
client.run(token)
