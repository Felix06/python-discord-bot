from dataclasses import dataclass
from typing import List

@dataclass
class Song:
    name: str
    def __repr__(self):
        representation = self.name
        return self.name

@dataclass
class Playlist:
    name: str
    songs: List[Song]

@dataclass
class PlaylistDbData:
    playlists: List[Playlist]
    