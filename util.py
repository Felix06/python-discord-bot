def get_matches_in_list_for_songs(origin_list, match):
    new_list = []
    for el in origin_list:
        if match.upper() in el.name.upper():
            new_list.append(el)
    return new_list