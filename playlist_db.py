import os
from song_dataclasses import *
from dataclasses_serialization.json import JSONSerializer
import json

def get_local_music_file_as_songs():
    music_folder = "music_folder"
    dir_path = os.path.join(os.path.dirname(__file__), music_folder)
    localfiles_list = []
    for root, dirs, files in os.walk(dir_path):
        for file in files:
            localfiles_list.append(Song(str(file)))    
    return localfiles_list

class PlaylistDb:

    def __init__(self):
        self.playlist_db_file = os.path.join(os.path.dirname(__file__), "playlist_db\\playlist_db.json")
        self.playlist_data = PlaylistDbData([])
        self.loadDb()
        self.update_local_playlist()

    def loadDb(self):
        with open(self.playlist_db_file, "r") as db_f:
            self.playlist_data = JSONSerializer.deserialize(PlaylistDbData, json.loads(db_f.read()))
    
    def update_local_playlist(self):
        self.playlist_data.playlists[0] = Playlist("local_playlist", get_local_music_file_as_songs())
        self.dumpSavedPlaylist()

    def dumpSavedPlaylist(self):
        with open(self.playlist_db_file, "w+") as db_f:
            db_f.write(json.dumps(JSONSerializer.serialize(self.playlist_data)))

    def get_list_of_song_with_playlist_number(self, playlist_number):
        playlist = self.playlist_data.playlists[playlist_number - 1]
        return playlist.songs

    def delete_playlist_with_number(self, num):
        self.playlist_data.playlists.pop(num - 1)
        self.dumpSavedPlaylist()